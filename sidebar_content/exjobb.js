document.write('
<div class="slide">
	<h1>Teknik-SM 2013</h1><hr>
	<p>T�vla i Teknik-SM - sveriges kanske smartaste och mest kreativa m�sterskap.
	<br>Kvalt�vlingen �ger rum den 14 oktober p� Ume� Universitet. Anm�l ett f�rdigt lag eller enskilt till amanuenserna. </p>
	<p><b>Exempelfr�ga:</b> G�sper har f�tt l�na en klockradio som den ovan av sin kamrat. Varje siffra i displayen 
	byggs upp av 7 segment. De kan vara lysande eller sl�ckta. N�r G�sper fick l�na klockradion fick han h�ra att ett av segmenten 
	p� en av minutsiffrorna inte fungerade, utan alltid var sl�ckt. Mitt i natten vaknar G�sper och kastar en blick p� 
	klockan. Tyv�rr hade han gl�mt vilket segment som var trasigt, men det han ser �r ett verkligt klockslag. F�r hur m�nga olika 
	minuttal kan G�sper vara s�ker p� att vad klockan visar verkligen �r sant?</p>
</div>
');